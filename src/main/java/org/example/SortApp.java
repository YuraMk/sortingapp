package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Sorting Application
 *
 */
public class SortApp
{
    public static void main( String[] args )
    {
        SortApp sortApp = new SortApp();
        int[] arr = createList();
        int[] arr1 = sortApp.sort(arr);
        sortApp.printArr(arr1);
    }

    public void printArr(int[] arr){
        for (int j : arr) {
            System.out.print(j + " ");
        }
        System.out.println();
    }

    public int[] sort(int[] arr) {
        if (arr.length > 10) {
            int[] arr1 = new int[10];
            System.arraycopy(arr, 0, arr1, 0, 10);
            Arrays.sort(arr1);
            return arr1;
        } else {
            Arrays.sort(arr);
            return arr;
        }
    }

    private static int[] createList(){
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> arr = new ArrayList<>();
        int count = 1;
        while(count <= 10 && sc.hasNextInt()){
            int n = sc.nextInt();
            arr.add(n);
            count++;
        }
        int[] newArr = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            newArr[i] = arr.get(i);
        }
        return newArr;
    }
}
