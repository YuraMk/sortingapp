package org.example;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for sorting app.
 */
public class SortAppTest{
    SortApp sortApp = new SortApp();
    @Test
    public void zeroArgumentCaseTest() {
        int[] arr = {};
        int[] expected = {};
        assertArrayEquals(expected, sortApp.sort(arr));
    }

    @Test
    public void oneArgumentCaseTest(){
        int[] arr = {1};
        int[] expected = {1};
        assertArrayEquals(expected, sortApp.sort(arr));
    }

    @Test
    public void tenArgumentsCaseTest(){
        int[] arr = {4, 0, 8, 5, 7, 3, 75, 20, 11, 1};
        int[] expected = {0, 1, 3, 4, 5, 7, 8, 11, 20, 75};
        assertArrayEquals(expected, sortApp.sort(arr));
    }

    @Test
    public void moreThanTenCaseTest(){
        int[] arr = {4, 0, 8, 5, 7, 3, 75, 20, 11, 1, 21, 2, 6};
        int[] expected = {0, 1, 3, 4, 5, 7, 8, 11, 20, 75};
        assertArrayEquals(expected, sortApp.sort(arr));
    }
}