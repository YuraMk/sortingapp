package org.example;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
@RunWith(Parameterized.class)
public class ParametrizedSortAppTest {

    int[] arr;
    int[] expected;

    SortApp sortApp = new SortApp();

    public ParametrizedSortAppTest(int[] arr, int[] expected){
        this.arr = arr;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[] {7, 54, 2, 1, 0},new int[]{0, 1, 2, 7, 54}},
                {new int[] {4}, new int[] {4}},
                {new int[]{10, 9, 8, 5, 6, 6, 1, 3, 2, -1}, new int[]{-1, 1, 2, 3, 5, 6, 6, 8, 9, 10}},
                {new int[]{7, 7, 6, 6}, new int[] {6, 6, 7, 7}}
        });
    }

    @Test
    public void testParametrizedTests(){
        assertArrayEquals(expected, sortApp.sort(arr));
    }
}